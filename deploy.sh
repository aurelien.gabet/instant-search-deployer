#!/bin/bash
set -e

ssh-keygen -R $(minikube ip)
ssh  -o "StrictHostKeyChecking no" -t -i ~/.minikube/machines/minikube/id_rsa docker@$(minikube ip) "echo fs.inotify.max_user_watches=524288 | sudo tee /etc/sysctl.conf && sudo sysctl -p"
ansible-playbook orchestrate.yaml
chmod +x kube_files/displayurl.sh && ./kube_files/displayurl.sh
