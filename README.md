
# Instant-search-demo deployer

This repo aims to package and deploy the **Instant-search-demo** from **Algolia**. The deployment is divided into two parts: 
* The application is packaged with **Docker**
* The Docker Image is instanciated in **Minikube** ( a tool to run a tiny Kubernetes cluster locally)

All the deployment orchestration is managed by **Ansible** (mainly because of its jinja templating).

## Pre-requisites

The deployment of Instant-search-demo was tested on Linux and macOs.

**Ansible**
To be sure that we are in the same environment, configure a **virtualenv**:

* Installation of pip https://pip.pypa.io/en/stable/installing/ (tested with Python 2.7.15)
* Installation of virtualenv: `pip install virtualenv`
* Create a **virtualenv** in the working directory, then activate it (on each terminal session you use):

```
virtualenv venv
source venv/bin/activate
```
* Installation of the dependencies in the requirements.txt:
```
pip install -r requirements.txt
```

**Minikube and kubectl**

  *Disclaimer*: if you deploy from a VM you must ensure that it support nested virtualization

Install **Minikube** (with requirements) and the **kubectl** client following this documentation: https://kubernetes.io/docs/tasks/tools/install-minikube/

* Start minikube with kubernetes version v1.12.1 and once ready, enable ingress controller to access the applications:

*Optional*: if you already have minikube and want to begin from a fresh instance, I recommend deleting the VM with the command: `minikube delete`.

```
minikube start --kubernetes-version v1.12.1
minikube addons enable ingress
```

## Deployment of the application

From the working directory, run `./deploy.sh`

The script will do the following actions:

 - patch minikube inotify
 - check the prerequisites
 - clone the algolia application to package it in a Docker container
 - instanciate yaml files whitch describes the Kubernetes topology
 - instanciate the topology in Kubernetes
 - Run infrastructure tests
 - Display the URL of the application just deployed

## Access the application

The URL to access the application should be displayed at the end deployment script `deploy.sh`. 
Otherwise, you can find the URL with this command: `kubectl get ing -n algolia` (if you did not change the default variables configuration)

## Versioning of the container image

The application is packaged and deployed with version **1.0**, it can be changed in the `group_vars/all/all.yaml` file (see **image_version**).

You can then **re-deploy** the application running the `./deploy.sh` with **zero downtime deployment** (since it is using a Kubernetes Rolling Update strategy).

## TODO

- Pass API informations through environment variables as kubernetes secrets (currently blocked because browser-sync prevents me from accessing to process.env.VARS)
- Automate the push of the index with my personal root api key